FROM python:3.11-slim-buster

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install system dependencies
RUN apt-get update && apt-get -y install netcat gcc postgresql && apt-get clean

# install dependencies
RUN pip install --upgrade pip
RUN pip install pipenv
COPY ./Pipfile.lock /usr/src/app/Pipfile.lock
COPY ./Pipfile /usr/src/app/Pipfile
RUN pipenv install --system --deploy --ignore-pipfile
RUN pipenv install --system --deploy --ignore-pipfile --dev


# add app
COPY . .

