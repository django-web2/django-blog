from django.urls import path
from . import views

app_name = "comments"
urlpatterns = [
    path("<int:post_id>/comment/", views.post_comment_vew, name="post_comment"),
]

