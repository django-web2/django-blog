from django.shortcuts import render, get_object_or_404
from django.views.decorators.http import require_POST

from apps.blog.models import Post
from apps.comments.forms import CommentForm


# ...
@require_POST
def post_comment_vew(request, post_id):
    post = get_object_or_404(Post, id=post_id, status=Post.Status.PUBLISHED)
    comment = None
    # A comment was posted
    form = CommentForm(data=request.POST)
    if form.is_valid():
        # Create a Comment object without saving it to the database
        comment = form.save(commit=False)
        # Assign the post to the comment
        comment.post = post
        # Save the comment to the database
        comment.save()
    return render(request, 'comments/comment.html', {'post': post, 'form': form, 'comment': comment})


