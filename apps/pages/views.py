from django.shortcuts import render
from apps.blog.models import Category
# Create your views here.


def home_view(request):
    categories = Category.objects.all()
    context = {"categories": categories}
    return render(request, "pages/home.html", context=context)


def about_view(request):
    categories = Category.objects.all()
    context = {"categories": categories}
    return render(request, "pages/about_me.html", context=context)