from django.core.mail import send_mail
from django.shortcuts import get_object_or_404, render

from config.settings.base import EMAIL_HOST_USER

from ..blog.models import Post
from .forms import EmailPostForm, EmailContactForm


def post_share_view(request, post_id):
    # Retrieve post by id
    post = get_object_or_404(Post, id=post_id, status=Post.Status.PUBLISHED)
    sent = False
    if request.method == "POST":
        # Form was submitted
        form = EmailPostForm(request.POST)
        if form.is_valid():
            # Form fields passed validation
            cd = form.cleaned_data
            post_url = request.build_absolute_uri(post.get_absolute_url())
            subject = f"{cd['name']} vous recommende de lire {post.title}"
            message = (
                f"Lire {post.title} ici {post_url}\n\n"
                f"Les commentaires de {cd['name']}: {cd['comments']}"
            )
            send_mail(subject, message, EMAIL_HOST_USER, [cd["to"]])
            sent = True
    else:
        form = EmailPostForm()
    return render(
        request, "emails/share.html", {"post": post, "form": form, "sent": sent}
    )


def post_email_view(request):
    """Let people contact me by email"""
    sent = False
    if request.method == "POST":
        form = EmailContactForm(request.POST)
        if form.is_valid():
            subject = form.cleaned_data["subject"]
            from_email = form.cleaned_data["email"]
            message = form.cleaned_data["comments"]
            send_mail(subject, message, from_email, [EMAIL_HOST_USER])
            sent = True
    else:
        form = EmailContactForm()
    return render(request, "emails/contact.html", context={"form": form, "sent": sent})
