from django import forms
from crispy_forms.helper import FormHelper

from apps.emails.form_layouts import EmailContactLayout, EmailPostLayout


class EmailPostForm(forms.Form):
    name = forms.CharField(max_length=255, label="")
    email = forms.EmailField(label="")
    to = forms.EmailField(label="")
    comments = forms.CharField(required=False, widget=forms.Textarea, label="")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = EmailPostLayout()


class EmailContactForm(forms.Form):
    subject = forms.CharField(max_length=300, label="")
    email = forms.EmailField(label="")
    comments = forms.CharField(widget=forms.Textarea, label="")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = EmailContactLayout()
