from django.urls import path

from . import views

app_name = "emails"

urlpatterns = [
    path("<int:post_id>/share/", views.post_share_view, name="post_share"),
    path("contact/", views.post_email_view, name="contact"),
]
