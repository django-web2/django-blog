from crispy_forms.layout import Layout, Div, Row, Column, Fieldset, Field


class EmailContactLayout(Layout):
    """Styles for EmailContactFom"""

    def __init__(self):
        super().__init__(
            Fieldset(
                "Email",
                Row(
                    Column(Field("email", placeholder="Votre email"), css_class="col-lg-6 col-sm-12"),
                ),
                Row(
                    Column(Field("subject", placeholder="Objet"), css_class="col-lg-6 col-sm-12"),
                ),
                Row(
                    Column(Field("comments", placeholder="Votre message"), css_class="mb-5"),
                ),
            ),
        )


class EmailPostLayout(Layout):
    """Styles EmailPostForm"""

    def __init__(self):
        super().__init__(
            Fieldset(
                Row(
                    Column(Field("name", placeholder="Votre nom"), css_class="col-lg-6 col-sm-12 mb-2"),
                ),
                Row(
                    Column(Field("email", placeholder="Votre email"), css_class="col-lg-6 col-sm-12 mb-2"),
                ),
                Row(
                    Column(Field("to", placeholder="Email du destinataire"), css_class="col-lg-6 col-sm-12 mb-2"),
                ),
                Row(
                    Column(Field("comments", placeholder="Votre message"), css_class="mb-5")
                ),
            ),
        )
