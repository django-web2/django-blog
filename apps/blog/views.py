from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.db.models import Count
from django.shortcuts import get_object_or_404, render
from django.contrib.postgres.search import SearchVector, SearchRank, SearchQuery

from taggit.models import Tag

from .models import Post, Category
from ..comments.forms import CommentForm
from apps.blog.forms import SearchForm


def post_list_view(request, tag_slug=None, category_slug=None):
    """Display the list of all published post"""
    post_list = Post.published.all()
    tag = None
    category = None
    if tag_slug:
        tag = get_object_or_404(Tag, slug=tag_slug)
        post_list = post_list.filter(tags__in=[tag])
    if category_slug:
        category = get_object_or_404(Category, slug=category_slug)
        post_list = post_list.filter(category=category)
    paginator = Paginator(post_list, 1)
    page_number = request.GET.get("page", 1)
    try:
        posts = paginator.page(page_number)
    except EmptyPage:
        posts = paginator.page(paginator.num_pages)
    except PageNotAnInteger:
        posts = paginator.page(1)
    context = {"posts": posts, "page": posts, "tag": tag, "category": category}
    if request.htmx:
        return render(request, "blog/list-elements.html", context=context)
    return render(request, "blog/list.html", context=context)


def post_detail_view(request, year, month, day, post):
    post = get_object_or_404(
        Post,
        status=Post.Status.PUBLISHED,
        slug=post,
        publish__year=year,
        publish__month=month,
        publish__day=day,
    )
    # List of active comments for this post
    comments = post.comments.filter(active=True)
    # Form for users to comment
    form = CommentForm()
    # List of similar posts
    post_tags_ids = post.tags.values_list("id", flat=True)
    similar_posts = Post.published.filter(tags__in=post_tags_ids).exclude(id=post.id)
    similar_posts = similar_posts.annotate(same_tags=Count("tags")).order_by("-same_tags", "-publish")[:4]

    context = {'post': post,
               'comments': comments,
               'form': form,
               "similar_posts": similar_posts,}
    return render(request, 'blog/detail.html', context=context)


def post_search(request):
    if request.method == "POST":
        query = request.POST["searched"]
        search_vector = SearchVector("title", config="french", weight="A") + SearchVector("body", weight="B")
        search_query = SearchQuery(query, config="french")
        results = Post.published.annotate(search=search_vector, rank=SearchRank(search_vector, search_query)).filter(
            rank__gte=0.3).order_by("-rank")
        print(results)
        return render(request, "blog/search.html", {"query": query, "results": results})
