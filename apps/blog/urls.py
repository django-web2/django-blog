from django.urls import path

from . import views
from apps.rss.feeds import LatestPostsFeed

app_name = "blog"
urlpatterns = [
    path("", views.post_list_view, name="post_list"),
    path(
        "detail/<int:year>/<int:month>/<int:day>/<slug:post>/",
        views.post_detail_view,
        name="post_detail",
    ),
    path("category/<slug:category_slug>/", views.post_list_view, name="post_list_by_category"),
    path("tag/<slug:tag_slug>/", views.post_list_view, name="post_list_by_tag"),
    path("feed/", LatestPostsFeed(), name="post_feed"),
    path("search/", views.post_search, name="post_search")
]
