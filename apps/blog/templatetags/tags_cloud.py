from django import template
from taggit.models import Tag

register = template.Library()


@register.inclusion_tag("blog/tag_cloud.html")
def sidebar_tag_cloud():
    all_tags = Tag.objects.all()
    return {"tags": all_tags}
