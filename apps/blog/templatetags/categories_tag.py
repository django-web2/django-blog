from django import template
from apps.blog.models import Category

register = template.Library()


@register.inclusion_tag("blog/categories.html")
def get_all_categories():
    all_cats = Category.objects.all()
    return {"categories": all_cats}
