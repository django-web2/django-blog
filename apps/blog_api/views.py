from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.shortcuts import get_object_or_404
from taggit.models import Tag
from apps.blog.models import Post, Category
from .serializers import PostSerializer


class PostsList(APIView):
    def get(self, request):
        posts = Post.objects.all()
        serializer = PostSerializer(posts, many=True)
        return Response(serializer.data)


class PostListByTag(APIView):
    def get_object_list(self, tag_slug):
        try:
            return Tag.objects.get(slug=tag_slug)
        except Tag.DoesNotExist:
            return Response(
                {"error": "Ce tag n'existe pas"}, status=status.HTTP_404_NOT_FOUND
            )

    def get(self, request, slug):
        tag = self.get_object_list(slug)
        posts = Post.objects.filter(tags=tag)
        serializers = PostSerializer(posts, many=True)
        return Response(serializers.data)


class PostListByCategory(APIView):
    def get_object_list(self, category_slug):
        try:
            return Category.objects.get(slug=category_slug)
        except Category.DoesNotExist:
            return Response(
                {"error": f"La catégorie demandé n'existe pas."}, status=status.HTTP_404_NOT_FOUND
            )

    def get(self, request, slug):
        category = self.get_object_list(slug)
        posts = Post.objects.filter(category=category)
        serializers = PostSerializer(posts, many=True)
        return Response(serializers.data)


class PostDetail(APIView):
    def get_object(self, slug):
        try:
            return Post.objects.get(slug=slug)
        except Post.DoesNotExist:
            raise Http404

    def get(self, request, slug):
        post = self.get_object(slug)
        serializer = PostSerializer(post)
        return Response(serializer.data)
