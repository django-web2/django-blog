from rest_framework import serializers
from taggit.models import Tag
from apps.blog.models import Post


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ['id', 'name']


class PostSerializer(serializers.ModelSerializer):
    tags = serializers.SerializerMethodField()

    class Meta:
        model = Post
        fields = "__all__"

    def get_tags(self, obj):
        return TagSerializer(obj.tags.all(), many=True).data