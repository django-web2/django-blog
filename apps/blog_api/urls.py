from django.urls import path

from .views import PostDetail, PostsList, PostListByCategory, PostListByTag

app_name = "blog_api"
urlpatterns = [
    path("api/post/<str:slug>/", PostDetail.as_view(), name="post_detail"),
    path("api/posts/", PostsList.as_view(), name="post_list"),
    path("api/posts/category/<str:slug>/", PostListByCategory.as_view(), name="post_list_by_category"),
    path("api/posts/tag/<str:slug>/", PostListByTag.as_view(), name="post_list_by_tag"),
]
