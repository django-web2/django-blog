import os

from celery import Celery

# Celery config
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings.base")
app = Celery("config")
app.config_from_object("django.conf:setticngs", namespace="CELERY")
app.autodiscover_tasks()