"""Django Blog URL Configuration."""
from django.contrib import admin
from django.urls import include, path
from django.conf.urls.static import static
from django.conf import settings
from django.contrib.sitemaps.views import sitemap
from apps.blog.sitemaps import PostSitemap

sitemaps = {
    "post": PostSitemap
}

urlpatterns = [
    path("admin/", admin.site.urls),
    path("blog/", include("apps.blog.urls")),
    path("emails/", include("apps.emails.urls")),
    path("comments/", include("apps.comments.urls")),
    path("sitemap.xml", sitemap, {"sitemaps": sitemaps}, name="django.contrib.sitemaps.views.sitemap"),
    path("blog_api/", include("apps.blog_api.urls")),
    path("", include("apps.pages.urls"))
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
