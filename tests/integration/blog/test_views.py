from pprint import pprint

import pytest
from django.contrib.auth import get_user_model
from django.urls import reverse
from pytest_django.asserts import assertTemplateUsed
from taggit.models import Tag
from django.utils.html import strip_tags
from apps.blog.models import Post, Category


@pytest.fixture
def user():
    return get_user_model().objects.create_user(
        username="testuser",
        email="testuser@example.com",
        password="testpass"
    )


@pytest.fixture
def category():
    return Category.objects.create(name="test category", slug="test_category")


@pytest.fixture
def post(user, category):
    post = Post.objects.create(
        title='Test post',
        slug='test-post',
        author=user,
        body='Lorem ipsum dolor sit amet',
        status=Post.Status.PUBLISHED,
        category=category,
    )
    return post


@pytest.fixture
def tagger():
    tag1 = Tag.objects.create(name="tag1")
    tag2 = Tag.objects.create(name="tag2")
    return [tag1, tag2]


@pytest.mark.django_db
def test_post_list_view_returns_200_ok(client):
    url = reverse("blog:post_list")
    response = client.get(url)
    assert response.status_code == 200


@pytest.mark.django_db
def test_post_list_view_uses_correct_template(client):
    url = reverse("blog:post_list")
    response = client.get(url)
    assertTemplateUsed(response, "blog/list.html")


@pytest.mark.django_db
def test_post_list_view_returns_only_published_posts(client, post):
    post = post
    url = reverse("blog:post_list")
    print(url)
    response = client.get(url)
    assert post in response.context["posts"]


@pytest.mark.django_db
def test_post_list_view_not_returns_unpublished_posts(client, add_unpublished_post):
    url = reverse("blog:post_list")
    response = client.get(url)

    post_unpublished = add_unpublished_post(
        title="post_test",
        slug="post_slug_test",
        author="testuser",
        body="body_test",
        status=Post.Status.DRAFT,
        category="category_test",
    )
    assert post_unpublished not in response.context["posts"]


@pytest.mark.django_db
def test_post_list_view_return_post_with_correct_tag(client, post, tagger):
    post = post
    post.tags.set(tagger)
    print("VOila mes tags: ", post.tags.all()[0].slug)
    url = reverse("blog:post_list")
    response = client.get(url)
    print(response.context)
    assert response.status_code == 200
    assert tagger[0].name in str(response.context)
    # assert "tags2" in response.context


@pytest.mark.django_db
def test_post_list_view_pagination_system(client, post):
    """Check if there are the right number of posts in the page"""
    for i in range(7):
        post = post

    url = reverse("blog:post_list") + "?page=1"
    response = client.get(url)
    assert "Post 0" in strip_tags(str(response.content))
    assert "Post 1" in strip_tags(str(response.content))
    assert "Post 2" in strip_tags(str(response.content))
    assert "Post 3" in strip_tags(str(response.content))
    assert "Post 4" in strip_tags(str(response.content))
    assert "Post 5" in strip_tags(str(response.content))
    assert "Post 6" not in strip_tags(str(response.content))


@pytest.mark.django_db
def test_post_detail_view_returns_200_ok(client, post):
    post = post
    url = reverse(
        "blog:post_detail",
        args=[post.publish.year, post.publish.month, post.publish.day, post.slug],
    )
    response = client.get(url)
    assert response.status_code == 200


@pytest.mark.django_db
def test_post_detail_view_uses_correct_template(client, post):
    post = post

    url = reverse(
        "blog:post_detail",
        args=[post.publish.year, post.publish.month, post.publish.day, post.slug],
    )
    response = client.get(url)
    assertTemplateUsed(response, "blog/detail.html")


@pytest.mark.django_db
def test_post_detail_view_returns_correct_post(client, post):
    post = post
    url = reverse(
        "blog:post_detail",
        args=[post.publish.year, post.publish.month, post.publish.day, post.slug],
    )
    response = client.get(url)
    assert response.context["post"] == post


@pytest.mark.django_db
def test_list_template_posts_in_context(client, post):
    """Vérifie le contenu du template blog/list.html"""
    post_1 = post

    post_2 = Post.objects.create(
        title="post_test_2",
        slug="post_slug_test_2",
        author=get_user_model().objects.create(username="testuser2"),
        body="body_test_2",
        status=Post.Status.PUBLISHED,
        category=Category.objects.create(name="category_test", slug="catgory_test_slug"),
    )
    response = client.get(reverse("blog:post_list"))

    # 3. Vérification de la réponse.
    assert "posts" in response.context
    assert len(response.context["posts"]) > 1


@pytest.mark.django_db
def test_post_detail_template(client, post):
    post = post
    url = reverse(
        "blog:post_detail",
        args=[post.publish.year, post.publish.month, post.publish.day, post.slug],
    )
    response = client.get(url)
    assert response.status_code == 200
    assert "Test post" in str(response.content)
    assert "testuser" in str(response.content)
    assert "Lorem ipsum dolor sit amet" in str(response.content)
