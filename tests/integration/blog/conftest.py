import pytest
from django.contrib.auth import get_user_model

from apps.blog.models import Post, Category


@pytest.fixture(scope="function")
def add_published_post():
    def _add_post(title, slug, author, body, status, category):
        post = Post.objects.create(
            title="post_test",
            slug="post_slug_test",
            author=get_user_model().objects.create(username="testuser"),
            body="body_test",
            status=Post.Status.PUBLISHED,
            category=Category.objects.create(name="category_test", slug="catgory_test_slug")
        )
        return post

    return _add_post


@pytest.fixture(scope="function")
def add_unpublished_post():
    def _add_post(title, slug, author, body, status, category):
        post = Post.objects.create(
            title="post_test",
            slug="post_slug_test",
            author=get_user_model().objects.create(username="testuser"),
            body="body_test",
            status=Post.Status.DRAFT,
            category=Category.objects.create(name="category_test", slug="catgory_test_slug")
        )
        return post

    return _add_post
