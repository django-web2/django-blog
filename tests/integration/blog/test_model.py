import pytest
from django.contrib.auth import get_user_model

from apps.blog.models import Post, Category


@pytest.mark.django_db
def test_post_model():
    """Test if creates post model works"""
    post = Post(
        title="post_test",
        slug="post_slug_test",
        author=get_user_model().objects.create(username="testuser"),
        body="body_test",
        status=Post.Status.PUBLISHED,
        category=Category.objects.create(name="category_test", slug="catgory_test_slug")
    )
    post.save()

    assert post.title == "post_test"
    assert post.slug == "post_slug_test"
    assert post.author.username == "testuser"
    assert post.body == "body_test"
    assert post.status == "PB"
    assert post.category.name == "category_test"
    assert post.created
    assert post.updated
    assert post.publish
    assert str(post) == post.title


@pytest.mark.django_db
def test_get_only_published_post_from_published_manager():
    """test if the query ot get only published post works"""
    post = Post.objects.create(
        title="post_test",
        slug="post_slug_test",
        author=get_user_model().objects.create(username="testuser"),
        body="body_test",
        status=Post.Status.PUBLISHED,
        category=Category.objects.create(name="category_test", slug="catgory_test_slug")
    )
    post.save()

    published_posts = Post.objects.filter(status=Post.Status.PUBLISHED)
    published_manager_posts = Post.published.all()

    assert len(published_manager_posts) == len(published_posts)
    assert all(post in published_manager_posts for post in published_posts)


@pytest.mark.django_db
def test_unpublished_post_are_not_return_from_published_manager():
    """test if the query ot get only published post works"""
    unpublished_post = Post.objects.create(
        title="post_test",
        slug="post_slug_test",
        author=get_user_model().objects.create(username="testuser"),
        body="body_test",
        status=Post.Status.DRAFT,
        category=Category.objects.create(name="category_test", slug="catgory_test_slug")
    )
    unpublished_post.save()

    published_manager_posts = Post.published.all()

    assert unpublished_post not in published_manager_posts
