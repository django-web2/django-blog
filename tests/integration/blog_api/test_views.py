import pytest
from django.urls import reverse
from django.contrib.auth import get_user_model
from apps.blog.models import Category, Post
from rest_framework.test import APIClient
from taggit.models import Tag
from apps.blog_api.serializers import PostSerializer


@pytest.fixture
def user():
    return get_user_model().objects.create_user(
        username="testuser",
        email="testuser@example.com",
        password="testpass"
    )


@pytest.fixture
def category():
    return Category.objects.create(name="test category", slug="test_category")

@pytest.fixture
def tagger():
    tag1 = Tag.objects.create(name="tag1")
    tag2 = Tag.objects.create(name="tag2")
    return [tag1, tag2]

@pytest.fixture
def post(user, category):
    post = Post.objects.create(
        title='Test post',
        slug='test-post',
        author=user,
        body='Lorem ipsum dolor sit amet',
        status=Post.Status.PUBLISHED,
        category=category,
    )
    return post


@pytest.mark.django_db
def test_get_all_posts(client: APIClient, post, user, category):
    post_1 = post

    post_2 = Post.objects.create(
        title='Test post 2',
        slug='test-post-2',
        author=user,
        body='Lorem ipsum dolor sit amet',
        status=Post.Status.PUBLISHED,
        category=category,
    )

    url = reverse("blog_api:post_list")
    response = client.get(url)
    assert response.status_code == 200
    assert response.data[1]["title"] == post_1.title
    assert response.data[0]["title"] == post_2.title


@pytest.mark.django_db
def test_get_posts_by_categories(client:APIClient, post, user):
    post_2 = Post.objects.create(
        title='Test post 2',
        slug='test-post-2',
        author=user,
        body='Lorem ipsum dolor sit amet',
        status=Post.Status.PUBLISHED,
        category=Category.objects.create(name="test category 2", slug="test_category_2")
    )
    url = reverse("blog_api:post_list_by_category", kwargs={"slug": post.category.slug})
    response = client.get(url)

    assert response.status_code == 200
    assert response.data[0]["title"] == post.title
    assert post_2.title not in response.data

@pytest.mark.django_db
def test_get_posts_by_one_tag(client: APIClient, post, tagger):
    post.tags.set(tagger)
    print(post.tags.slugs())
    url = reverse("blog_api:post_list_by_tag", kwargs={"slug": post.tags.slugs()[0] })
    response = client.get(url)
    assert response.status_code == 200


@pytest.mark.django_db
def test_get_single_post(client: APIClient, post):
    post = post
    url = reverse("blog_api:post_detail", kwargs={"slug": post.slug})
    response = client.get(url)
    assert response.status_code == 200
    assert response.data["title"] == "Test post"


@pytest.mark.django_db
def test_get_single_post_incorrect_slug(client: APIClient, post):
    post = post
    url = reverse("blog_api:post_detail", kwargs={"slug": "foo"})
    response = client.get(url)
    assert response.status_code == 404


