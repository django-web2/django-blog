import pytest
from django.contrib.auth import get_user_model
from django.utils import timezone
from apps.blog.models import Post, Category
from taggit.models import Tag
from apps.blog_api.serializers import PostSerializer


@pytest.fixture
def user():
    return get_user_model().objects.create_user(
        username="testuser",
        email="testuser@example.com",
        password="testpass"
    )


@pytest.fixture
def category():
    return Category.objects.create(name="test category")


@pytest.fixture
def tags():
    tag1 = Tag.objects.create(name="tag1")
    tag2 = Tag.objects.create(name="tag2")
    return [tag1, tag2]


@pytest.mark.django_db
def test_post_serializer(tags, user, category):
    post = Post.objects.create(
        title="Test post",
        slug="test-post",
        author=user,
        category=category,
        body="This is a test post.",
        publish=timezone.now(),
        status=Post.Status.PUBLISHED
    )
    post.tags.set(tags)

    serializer = PostSerializer(post)
    assert serializer.data == {
        'id': post.id,
        'title': 'Test post',
        'slug': 'test-post',
        'author': user.id,
        'category': category.id,
        'body': 'This is a test post.',
        'publish': serializer.data['publish'],  # field type is DateTimeField, value is tested in test models
        'created': serializer.data['created'],  # field type is DateTimeField, value is tested in test models
        'updated': serializer.data['updated'],  # field type is DateTimeField, value is tested in test models
        'status': 'PB',
        'tags': [
            {'id': tags[0].id, 'name': 'tag1'},
            {'id': tags[1].id, 'name': 'tag2'}
        ]
    }
