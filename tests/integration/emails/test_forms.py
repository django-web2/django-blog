from apps.emails.forms import EmailPostForm


def test_email_post_form_valid():
    form = EmailPostForm(
        data={
            "name": "John Doe",
            "email": "johndoe@example.com",
            "to": "janedoe@example.com",
            "comments": "Check out this post!",
        }
    )
    assert form.is_valid() is True


def test_email_post_form_invalid():
    form = EmailPostForm(data={})
    assert form.is_valid() is False
    assert "name" in form.errors
    assert "email" in form.errors
    assert "to" in form.errors


def test_email_post_form_comments_not_required():
    form = EmailPostForm(
        data={
            "name": "John Doe",
            "email": "johndoe@example.com",
            "to": "janedoe@example.com",
        }
    )
    assert form.is_valid() is True
