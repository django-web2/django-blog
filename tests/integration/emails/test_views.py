import pytest

from django.urls import reverse
from django.core import mail
from django.contrib.auth import get_user_model
from pytest_django.asserts import assertTemplateUsed

from apps.blog.models import Post, Category
from apps.emails.forms import EmailPostForm


@pytest.fixture
def post():
    author = get_user_model().objects.create_user(
        username="testuser", password="password"
    )
    category = Category.objects.create(name="category_test", slug="catgory_test_slug")
    post = Post.objects.create(
        title='Test post',
        slug='test-post',
        author=author,
        body='Lorem ipsum dolor sit amet',
        status=Post.Status.PUBLISHED,
        category=category,
    )
    return post


@pytest.mark.django_db
def test_post_share_view_returns_200_ok(client, post):
    post = post
    url = reverse("emails:post_share", args=[post.id])
    response = client.get(url)
    assert response.status_code == 200


@pytest.mark.django_db
def test_post_list_view_uses_correct_template(client, post):
    post = post
    url = reverse("emails:post_share", args=[post.id])
    response = client.get(url)
    assertTemplateUsed(response, "emails/share.html")


@pytest.mark.django_db
def test_post_share_view_get_request(client, post):
    # create a test post
    post = post
    url = reverse('emails:post_share', args=[post.id])
    # test GET request
    response = client.get(url)
    assert 'form' in response.context
    assert isinstance(response.context['form'], EmailPostForm)


@pytest.mark.django_db
def test_post_share_view_post_request_with_invalid_data(client, post):
    post = post
    url = reverse('emails:post_share', args=[post.id])
    response = client.post(url, {'name': 'John Doe', 'email': 'johndoe@example.com'})
    assert 'form' in response.context
    assert isinstance(response.context['form'], EmailPostForm)
    assert len(mail.outbox) == 0  # no email should have been sent


@pytest.mark.django_db
def test_post_share_view_post_request_with_valid_data(client, post):
    post = post
    url = reverse('emails:post_share', args=[post.id])
    response = client.post(url, {
        'name': 'John Doe',
        'email': 'johndoe@example.com',
        'to': 'janedoe@example.com',
        'comments': 'Check out this post!'
    })
    assert 'form' in response.context
    assert isinstance(response.context['form'], EmailPostForm)
    assert len(mail.outbox) == 1  # one email should have been sent
