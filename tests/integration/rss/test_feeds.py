import pytest
from django.utils.html import strip_tags
from django.contrib.auth import get_user_model
from apps.blog.models import Post, Category
from apps.rss.feeds import LatestPostsFeed


@pytest.fixture
def post():
    author = get_user_model().objects.create_user(
        username="testuser", password="password"
    )
    category = Category.objects.create(name="category_test", slug="catgory_test_slug")
    post = Post.objects.create(
        title=f"Post",
        slug=f"post_slug_test",
        author=author,
        body=f"body_test",
        status=Post.Status.PUBLISHED,
        category=category,
    )
    return post


@pytest.mark.django_db
def test_items_return_all_last_published_posts():
    author = get_user_model().objects.create_user(
        username="testuser", password="password"
    )
    category = Category.objects.create(name="category_test", slug="catgory_test_slug")
    for i in range(7):
        post = Post.objects.create(
            title=f"Post {i}",
            slug=f"post_slug_test_{i}",
            author=author,
            body=f"body_test_{i}",
            status=Post.Status.PUBLISHED,
            category=category
        )
    feed = LatestPostsFeed()
    feed_items = feed.items()
    assert len(feed_items) == 5


@pytest.mark.django_db
def test_item_title(post):
    post = post
    feed = LatestPostsFeed()

    assert feed.item_title(post) == post.title


@pytest.mark.django_db
def test_item_description(post):
    post = post
    feed = LatestPostsFeed()

    assert strip_tags(feed.item_description(post)) == post.body


@pytest.mark.django_db
def test_item_pubdate(post):
    post = post
    feed = LatestPostsFeed()
    assert feed.item_pubdate(post) == post.publish
