import pytest
from django.core import mail
from django.urls import reverse
from django.contrib.auth import get_user_model
from pytest_django.asserts import assertTemplateUsed

from apps.blog.models import Post, Category
from apps.comments.forms import CommentForm


@pytest.fixture
def post():
    author = get_user_model().objects.create_user(
        username="testuser", password="password"
    )
    category = Category.objects.create(name="category_test", slug="catgory_test_slug")
    post = Post.objects.create(
        title='Test post',
        slug='test-post',
        author=author,
        body='Lorem ipsum dolor sit amet',
        status=Post.Status.PUBLISHED,
        category=category,
    )
    return post


@pytest.mark.django_db
def test_post_comment_view_returns_405_ok(client, post):
    post = post
    url = reverse("comments:post_comment", args=[post.id])
    response = client.get(url)
    assert response.status_code == 405


@pytest.mark.django_db
def test_post_comment_view_post_request_with_invalid_data(client, post):
    post = post
    url = reverse('comments:post_comment', args=[post.id])
    response = client.post(url, {'name': 'John Doe', 'email': 'johndoe@example.com'})
    assert 'form' in response.context
    assert isinstance(response.context['form'], CommentForm)


@pytest.mark.django_db
def test_post_share_view_post_request_with_valid_data(client, post):
    post = post
    url = reverse('comments:post_comment', args=[post.id])
    response = client.post(url, {
        'name': 'John Doe',
        'email': 'johndoe@example.com',
        "body": "test body",
    })
    assert 'form' in response.context
    assert isinstance(response.context['form'], CommentForm)