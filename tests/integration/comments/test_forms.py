from apps.comments.forms import CommentForm


def test_comment_form_valid():
    form = CommentForm(
        data={
            "name": "test_form",
            "email": "test@test.com",
            "body": "test body",
        }
    )
    assert form.is_valid() is True


def test_comment_form_invalid():
    form = CommentForm(
        data={}
    )
    assert form.is_valid() is False
    assert "name" in form.errors
    assert "email" in form.errors
    assert "body" in form.errors
