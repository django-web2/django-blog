import pytest

from django.contrib.auth import get_user_model

from apps.blog.models import Post, Category
from apps.comments.models import Comment


@pytest.fixture
def post():
    author = get_user_model().objects.create_user(
        username="testuser", password="password"
    )
    category = Category.objects.create(name="category_test", slug="catgory_test_slug")
    post = Post.objects.create(
        title='Test post',
        slug='test-post',
        author=author,
        body='Lorem ipsum dolor sit amet',
        status=Post.Status.PUBLISHED,
        category=category,
    )
    return post


@pytest.mark.django_db
def test_comment_model(post):
    """Test if creates post model works"""
    comment = Comment(
        post=post,
        name="test comment",
        email="exemple@exemple.com",
        body="Test comment body",
    )
    comment.save()

    assert comment.name == "test comment"
    assert comment.email == "exemple@exemple.com"
    assert comment.post.author.username == "testuser"
    assert comment.body == "Test comment body"
    assert comment.created
    assert comment.updated
    assert str(comment) == f'Comment by {comment.name} on {comment.post}'
