from django.urls import resolve, reverse

from apps.blog.views import post_detail_view, post_list_view


def test_post_list_view_resolves_to_correct_view():
    match = resolve("/blog/")

    assert match.func == post_list_view


def test_post_list_view_resolves_to_correct_view_name():
    match = resolve("/blog/")
    assert match.url_name == "post_list"


def test_post_detail_url():
    url = reverse("blog:post_detail", args=[2023, 3, 18, "test"])
    assert url == "/blog/detail/2023/3/18/test/"


def test_post_detail_view_resolves_to_correct_view():
    match = resolve("/blog/detail/2023/3/18/test/")
    assert match.func == post_detail_view
