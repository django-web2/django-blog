from django.urls import resolve, reverse

from apps.emails.views import post_share_view


def test_post_share_view_resolves_to_correct_view():
    match = resolve("/emails/1/share/")

    assert match.func == post_share_view


def test_post_share_view_url():
    url = reverse("emails:post_share", args=[1])
    assert url == "/emails/1/share/"