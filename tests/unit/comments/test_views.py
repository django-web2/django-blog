from django.urls import resolve, reverse
from apps.comments.views import post_comment_vew


def test_post_share_view_resolves_to_correct_view():
    match = resolve("/comments/1/comment/")

    assert match.func == post_comment_vew


def test_post_share_view_url():
    url = reverse("comments:post_comment", args=[1])
    assert url == "/comments/1/comment/"
